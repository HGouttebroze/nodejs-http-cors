const http = require('http')
const fs = require('fs')

/**
 * @param {module:http.ClientRequest} req
 * @param {module:http.ServerResponse} res
 */
const getPosts = function (req, res) {
  res.setHeader('Content-Type', 'application/json')
  res.write(JSON.stringify([1, 4, 3, 6]))
}

/**
 * @param {module:http.ClientRequest} req
 * @param {module:http.ServerResponse} res
 */
const postPosts = function (req, res) {
  res.setHeader('Content-Type', 'application/json')
  res.write(JSON.stringify({success: true}))
}

/**
 * @param {module:http.ClientRequest} req
 * @param {module:http.ServerResponse} res
 */
const cors = function (req, res) {
  if (req.headers.origin) {
    const origin = req.headers.origin
    res.setHeader('Access-Control-Allow-Origin', origin)
    res.setHeader('Access-Control-Allow-Credentials', 'true')
  }
  if (req.method === "OPTIONS") {
    res.setHeader('Access-Control-Allow-Headers', 'Accept, Content-Type')
  }
}

/**
 * @param {module:http.ClientRequest} req
 * @param {module:http.ServerResponse} res
 */
const getIndex = function (req, res) {
  let index = fs.readFileSync('index.html', {encoding: 'UTF8'})
  index = index.replace('$COOKIE', req.headers.cookie || '')
  res.writeHead(200, {
    'Content-Type': 'text/html',
    // 'Set-Cookie': `demo=0; Domain=.domain-a.localhost; HttpOnly; Max-Age=${60*60}`
  })
  res.write(index)
}

/**
 * @param {module:http.ClientRequest} req
 * @param {module:http.ServerResponse} res
 */
const httpHandler = function (req, res) {
  cors(req, res)
  console.log(req.method + ': ' + req.url)
  if (req.url === '/' && req.method === "GET") {
    getIndex(req, res)
  }
  if (req.url === '/posts' && req.method === "GET") {
    getPosts(req, res)
  }
  if (req.url === '/posts' && req.method === "POST") {
    console.log('Cookie : ', req.headers.cookie)
    res.setHeader('Set-Cookie', `demo=5; Domain=.domain-a.localhost; HttpOnly; Max-Age=${60*60}`)
    postPosts(req, res)
  }
  res.end()
}

http.createServer(httpHandler).listen(8000)
